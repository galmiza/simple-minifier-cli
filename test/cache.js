/*
 * Manages cache
 * 
 * Reads files from internet or cache if avaiable
 * 
 * 
 * Cached file structure: {name,nativeURL,md5,content}
 * 
 *  0: file not in cache
 *  1: file in cache but expired
 *  2: file in cache not expired
 *  3: file in cache but outdated
 *  4: file in cache and up to date
 *
 *
 * Requires cordova to be ready
 * Requires cordova plugin 'org.apache.cordova.file'
 * Requires CryptoJS
 */

var cache = (function() {
  var module = {};
  var files = []; // store our files

  // Generic error function from object
  module.error = function(err) {alert("ERR: "+JSON.stringify(err));}

  // Update file list
  module.filelist = function(path, success, error) {

  	// Define and run recursive function
	  var nextEntry = function(path, success, error) {
	    window.resolveLocalFileSystemURL(path,
	      function(entry) {
	        if (entry.isFile) {
	          if (!module.isInCache(entry.nativeURL))
              files.push({ name:entry.name, nativeURL: entry.nativeURL });
	          if (--remaining_browse==0) success(); // file processed
	        }
	        if (entry.isDirectory) {
	          var reader = entry.createReader();
	          reader.readEntries(function(res) {
              remaining_browse += res.length; // add entries to proceed
              for (var i=0,n=res.length;i<n;i++)
	              nextEntry(res[i].nativeURL, success);
              if (--remaining_browse==0) success(); // directory processed
	          }, error);
	        }
	      },
	      error
	    );
	  }
	  var remaining_browse = 1; // root dir to be processed
	  nextEntry(path, success, error);
	}

  // Loads the cached files, optionnally computes hash (md5) and load content
  module.load = function(success, error, opt) {
    module.filelist(cordova.file.dataDirectory, function() {

      // Process each file if needed
      if (opt.md5 || opt.content) {
        var remaining_read = files.length;
        for (var i=0,n=files.length;i<n;i++) 
	        module.readLocal(files[i].nativeURL, files[i],
	          function(res, ctx) {
	            if (opt.md5) ctx.md5 = CryptoJS.MD5(res).toString(CryptoJS.enc.Hex);
	            if (opt.content) ctx.content = res;
	            remaining_read--;
	            if (remaining_read==0) success(files);
	          },
	          error);
      }
    }, error);
  }

  // Reads a file from its FULL path
  module.readLocal = function(path, ctx, success, error) {
    window.resolveLocalFileSystemURL(path,
      function(fileEntry) {
        fileEntry.file(function(file) {
          var reader = new FileReader();
          reader.onloadend = function(evt) { success(evt.target.result, ctx); };
          reader.readAsText(file);
        }, error);
      }, error);
  }

  // Writes a file (relative path)
  // Returns content (for compatilibity with module.read)
  module.writeLocal = function(path, content, success, error) {

  	// Extracts dirname (http://planetozh.com/blog/2008/04/javascript-basename-and-dirname/)
  	var dirname = path.indexOf('/')==-1 ? '':path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');
    module.createPath(dirname,
    	function() {

				// Writes file
		    window.resolveLocalFileSystemURL(cordova.file.dataDirectory,
		      function(dir) {
		        dir.getFile(path, {create:true},
		          function(file) {
		            file.createWriter(function(writer) {
		              writer.onwrite = function(evt) {
		              	var nativeURL = cordova.file.dataDirectory+path;
		              	module.removeCache(nativeURL);
		              	files.push({
		              		nativeURL: nativeURL,
		              		content: content,
		              		md5: CryptoJS.MD5(content).toString(CryptoJS.enc.Hex)
		              	});
		              	success(content);
		              }
		              writer.onerror = error;
		              writer.write(content);
		            }, error);      
		          }, error);
		      }, error);
    	}, error
    );
  }

  // Returns file object in cache memory (not disk, full path)
  module.getFileInCache = function(path) {
   	var nativeURL = path;
    for (var i=0,n=files.length;i<n;i++) {
    	if (nativeURL==files[i].nativeURL)
    		return files[i];
    }
    return null; 	
  }
  // Removes a file in the cache (not from disk, full path)
  module.removeCache = function(path) {
  	var f = module.getFileInCache(path);
    if (f!=null) files.splice(files.indexOf(f),1);
  }

  // Check if a file is in the cache (full path)
  module.isInCache = function(path) {
    return (module.getFileInCache(path) != null);
  }

  // Recursivily creates a folder structure (relative path)
  module.createPath = function(path, success, error) {
  	var d = path.split('/'); // a/b/c => ['a','b','c']
  	if (d.length==0) success();
  	else {

  		// Prepare consecutive paths to create
	  	var pathes = [d[0]]; // ['a']
      for (var i=1;i<d.length;i++) // ['a','a/b','a/b/c']
      	pathes.push(pathes[i-1]+'/'+d[i]);

      // Define and run the recursive function
	  	var create = function(i) {
	  		window.resolveLocalFileSystemURL(cordova.file.dataDirectory,
		      function(dir) {
		        dir.getDirectory(pathes[i], {create:true},
		        	function() {
		        		if (i+1<pathes.length) create(i+1);
		        		else                   success();
		        	}, error);
		      }, error);
	  	}
	  	create(0);
	  }
  }

  // Returns file content
  // Can notify if download or update is required
  module.read = function(url, success, error) {
    alert(url);

  	// Parse url to get path in cache
  	var parser = document.createElement('a'); // idea from https://gist.github.com/jlong/2428561
    parser.href = url;
    var path = parser.pathname.substring(1); // remove first char /
    var nativeURL = cordova.file.dataDirectory+path;

    // Check in cache
    alert(nativeURL);
    var f = module.getFileInCache(nativeURL);
    alert(f);
    if (f!=null) if (f.content) return f.content;

    alert("dl needed");
    // Get from internet
  	$.ajax({
      url: url,
      success: function(res) {
      	module.writeLocal(path, res, success, error);
      },
      error: error
    });
  }

  return module;
})();