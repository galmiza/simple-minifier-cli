// Simpler minifier command line tool for javascript, html and css
// Copyright (c) 2015 Guillaume Adam  http://www.galmiza.net/

/*
 * Simpler minifier command line tool for javascript, html and css
 * Takes one or more filenames as parameter
 * Type detection is based on file extension
 * Output filename is computed by adding min. before the extension
 *  (example: a/b/c.js => a/b/c.min.js)
 *
 * Usage: node simple-minifier.js <file1> <file2> ... <fileN>
 */

// Import modules
var fs = require('fs');
var uglify = require('uglify-js');
var minify = require('html-minifier').minify;

// Process each command line argument
for (var i=2,n=process.argv.length;i<n;i++) {
  var fileIn = process.argv[i];

  // Process filename
  var ext = fileIn.substr(fileIn.lastIndexOf(".")); // get file extension
  var code = fs.readFileSync(fileIn, 'utf8'); // get file content
  var fileOut = fileIn.substr(0, fileIn.lastIndexOf(".")) + ".min"+ext; // compute output filename
  var minified;

  // Tweak css content to make it a valid html tag with <style>
  // So that it can be fully minified by html-minifier
  if (ext=='.css')
    code = '<style>'+code+'</style>';

  // Minify
  if (ext=='.js') 
    minified = uglify.minify(code, {fromString: true}).code;
  if (ext=='.html' || ext=='.htm' || ext=='.css')
    minified = minify(code, {  
      removeComments: true,
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true });

  // Remove css <style> tag that we previously added
  if (ext=='.css')
    minified = minified.match(/<style>(.*?)<\/style>/)[1];

  // Write output
  fs.writeFileSync(fileOut, minified); // write minified content
}

