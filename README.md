# Introduction

Simple-minifier-cli is an easy to use command line tool that lets you minify your web resources.  
It supports **javascript**, **html** and **css**.  

# Features

  * Supports javascript (.js), templates (.html, .htm) and style (.css) formats
  * Processes multi files at once *(see Usage)*
  * Automatically names minified file by adding .min to the original file extension *(e.g. a/b/c.js becomes a/b/c.min.js)*

# Requirements

Simple-minifier-cli is a [nodejs](https://nodejs.org/) application and relies on two modules: [uglify-js](https://github.com/mishoo/UglifyJS) and [html-minifier](https://github.com/kangax/html-minifier).

# Installation

1/ Copy simple-minifier-cli.js in your local drive  
2/ Install the required modules using the following commands  

```
$ cd /path/to/the/file
$ npm install uglify-js
$ npm install html-minifier
```

You are good to go!

# Usage

Simple-minifier-cli takes absolute or relative filenames as parameter.  
Each file is processed independently.  

*Example:*

```
$ node simple-minifier-cli.js js/ext/file1.js templates/file2.html css/file3.css
```

Will create three files: **js/ext/file1.min.js**, **templates/file2.min.html** and **css/file3.min.css**

# About

I started using the excellent online minifying tool [jscompress](http://jscompress.com/) but needed more automation to industrialize my projects.  
Feel free to integrate into you continuous deployment suite!
